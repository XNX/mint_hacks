# Ensuring root folder is preserved
alias chgrp='chgrp --preserve-root'
alias chmod='chmod --preserve-root'
alias chown='chown --preserve-root'

alias rm="echo 'WARNING: Deleting with rm is permanent! Use \\rm to confirm, or move to trash using: del'" # Original rm command
alias del="gio trash" # Delete command
alias ln="echo 'WARNING: ln creates hard links by default, which may be undesirable. Use \\ln to confirm (-s makes a symlink).'" # Making ln safer
alias sudo="sudo " # Fixing sudo alias

alias cd..='cd ..'
alias code='codium'
alias myip='curl myip.addr.tools 2> /dev/null || echo You are offline.'
alias unlicense='cp --preserve=all ~/Documents/UNLICENSE LICENSE && git add LICENSE && git mv LICENSE UNLICENSE && git commit -m "Unlicense"'

alias mkdir='mkdir -p'
